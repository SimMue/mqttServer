var http = require('http');
var httpServ = http.createServer(httpHandler);
var url = require('url');
var fs = require('fs');

httpServ.listen(8081);

// serves the normal html-files
function httpHandler(req, res){
	var q = url.parse(req.url, true);
	var filename = "." + q.pathname;

	fs.readFile(filename, function(err, data){
		if(err){
			console.log("requested file \'" + filename + "\' not found");
			res.writeHead(404, {'Content-Type': 'text/html'});
			return res.end("404 Not Found.");
		}
		res.writeHead(200);
		res.write(data);
		return res.end();
	});
}
