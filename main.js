var mqtt = require('mqtt');
var io = require('socket.io').listen(8080);

var mqttClient = mqtt.connect('mqtt://localhost:1883');

var subscriptions = [];
[{
	"topic" : "",
	"ids" : []
}];

var actualID = 0;

function getID(){
	console.log("processed id " + String(++actualID));
	return actualID;
}

function addSubscription(topic, id){
	var index = -1;
	for(i = 0; i < subscriptions.length; i++){
		if(subscriptions[i].topic == topic){
			index = i;
		}
	}
	if(index == -1){
		subscriptions.push({"topic" : topic,
							"ids" : [id]
						});
	}
	else{
		subscriptions[index].ids.push(id);
	}
}

function delSubscription(topic, id){
	for(i = 0; i < subscriptions.length; i++){
		if(subscriptions[i].topic == topic){
			if(subscriptions[i].ids.length == 1){
				subscriptions.splice(i, 1);
				return true;
			}
			else{
				for(k = 0; k < subscriptions[i].ids.length; k++){
					if(subscriptions[i].ids[k] == id){
						subscriptions[i].ids.splice(k, 1);
						return false;
					}
				}
			}
		}
	}
	return false;
}

/*
function delNSubscriptions(topics, id){
	var i = 0;
	var k = 0;

	while(i < topics.length){
		while(k < subscriptions.length){
			if(topics[i] == subscriptions[k].topic){
				topics.splice(i, 1);
				for(m = 0; m < subscriptions[k].ids.length; m++){
					if(subscriptions[k].ids == id){
						subscriptions[k].ids.splice(m, 1);
						m = subscriptions[k].ids.length;
					}
				}
				if(subscriptions[k].ids.length == 0){
					subscriptions.splice(k, 1);
				}
			}
			else if(k = subscriptions.length - 1){
				i++;
				k = 0;
			}
			else{
				k++;
			}
		}
	}
}*/

io.on('connection', function(socket){
	var id = getID();

	socket.emit('clientID', {
		'clientID' : String(id)
	});

	console.log("client with id " + id + " connected.");
	socket.on('subscribe', function(data){
		console.log('client with id ' + id + ' Subscribing to ' + data.topic);
		addSubscription(data.topic, id);
		socket.join(data.topic);
		mqttClient.subscribe(data.topic);
	});
	socket.on('unsubscribe', function(data){
		console.log('client with id ' + id + ' unsubscribing from ' + data.topic);
		if(delSubscription(data.topic, id) == true){
			mqttClient.unsubscribe(data.topic);
		}
		socket.leave(data.topic);
	});
	socket.on('disconnect', function(){
		console.log('client with id ' + id + ' closed the session');
	});
});

mqttClient.on('message', function(topic, payload){
	console.log(topic + ' = ' + payload.toString());
	io.sockets.in(topic).emit('mqtt',{
		'topic' : String(topic),
		'payload' : payload.toString()
	});
});

//mqttClient.publish("mqtt/demo", "hello world");
